# Deluge in a wireguard jail with WebUI available on local network

## Setup

1. Create a dedicated user and group to run docker-compose
1. Create a `.env` file in this directory setting
    1. `PUID=<newly created user's UID>`
    1. `PGID=<newly created user's group's GID>`
    1. `TZ=<Your timezone in "Region/City" format, e.g. Europe/Dublin>`
    1. `LOGLEVEL=<warn|info|error>` (any log level accepted by deluged)
1. Add your wireguard config file at `volumes/wireguard/wg0.conf` and add the line `PostDown = kill -9 1` to the end of the `[Interface]` section
1. Run `docker-compose up`, you'll see a message saying `Generating config files`
1. Hit CTRL-C once everything is up
1. Run `docker-compose up` again
1. Visit your server at port 8112 to get to the WebUI, the inital password is `deluge`
1. You should see your daemon container in the connection manager list, and it should be online; Connect to it, set up your download/completed/torrents folders, and enjoy

