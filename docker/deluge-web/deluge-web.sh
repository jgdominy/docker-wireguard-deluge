#!/bin/sh

shutdown() {
	echo "Gracefully shutting down"
	kill `cat /tmp/socat.pid`
	kill `cat /tmp/deluge-web.pid`
	exit 0
}

if [ -e .config/hostlist.conf ]; then
	rm -f shared/deluge-web.sock
	IFS=: read username pw priority < shared/deluge-web_auth
	sed -i -e '8s/".*"/'`cat shared/deluged_ip`'/' \
	       -e '10s/".*"/"'${username}'"/' \
	       -e '11s/".*"/"'${pw}'"/' .config/hostlist.conf
	deluge-web -L warn -c .config -d &
	echo -n $! > /tmp/deluge-web.pid
	socat UNIX-LISTEN:shared/deluge-web.sock,fork,mode=0777 TCP:127.0.0.1:8112 &
	echo -n $! > /tmp/socat.pid
	trap shutdown TERM
	while true; do sleep 1; done
else
	echo Generating config files
	exec deluge-web -L warn -c .config -d
fi
