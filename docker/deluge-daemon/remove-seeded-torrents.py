#!/usr/bin/python3
from deluge_client import DelugeRPCClient

print("remove-seeded-torrents: starting")
f = open('.config/auth', "r")
username = None
password = None
for line in f:
    if line.startswith('localclient'):
        username, password, _ = line.strip().split(':')

client = DelugeRPCClient('127.0.0.1', 58846, username, password)
client.connect()

torrents = client.call('core.get_torrents_status', {}, ['name', 'ratio', 'stop_at_ratio', 'stop_ratio'])
for torrent, status in torrents.items():
    if status[b'stop_at_ratio'] and status[b'ratio'] > status[b'stop_ratio']:
        print(f"Removing torrent: {torrent} {status[b'name']} ({status[b'ratio']}/{status[b'stop_ratio']}) ... ", end='')
        if client.call('core.remove_torrent', torrent, remove_data=False):
            print('OK')
        else:
            print('FAILED')
print("remove-seeded-torrents: completed")

