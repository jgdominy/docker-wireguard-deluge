#!/bin/sh

ip -j a s eth0 | jq '.[0].addr_info[0].local' > shared/deluged_ip
if [ -e .config/auth ]; then
	auth_line=`grep '^webui:' .config/auth`
	if ! [ -n "${auth_line}" ]; then
		auth_line=webui:`hexdump -n 16 -e '4/4 "%08x" 1 "\n"' /dev/random`:10
		echo ${auth_line} >> .config/auth
	fi
	echo ${auth_line} > shared/deluge-web_auth
	sed -i 's/"allow_remote": false/"allow_remote": true/' .config/core.conf
	exec /usr/bin/deluged -L ${LOGLEVEL} -c .config -d
else
	echo Generating config files
	exec /usr/bin/deluged -L ${LOGLEVEL} -c .config -d
fi
